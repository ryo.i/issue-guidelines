# Issue guidelines

This repository's [CONTRIBUTING.md](CONTRIBUTING.md) provides a generic set of
guidelines for developers to adapt and include in their own GitLab
repositories.

The section below can be included in your README to improve the visibility of
your contribution guidelines.

If you think this guide can be improved, please let me know.


## Contributing to this project

Anyone and everyone is welcome to contribute. Please take a moment to
review the [guidelines for contributing](CONTRIBUTING.md).

* [Bug reports](CONTRIBUTING.md#bugs)
* [Feature requests](CONTRIBUTING.md#features)
* [Merge requests](CONTRIBUTING.md#merge-requests)
